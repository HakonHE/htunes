package no.noroff.ytunes.repositories;

import no.noroff.ytunes.models.Customer;
import no.noroff.ytunes.models.CustomerCountry;
import no.noroff.ytunes.models.CustomerGenre;
import no.noroff.ytunes.models.CustomerSpender;

import java.util.ArrayList;

public interface CustomerRepository {
    ArrayList<Customer> getAllCustomers();
    Customer getCustomerById(String id);
    Customer getCustomerByName(String name);
    ArrayList<Customer> getCustomerPage(int limit, int offset);
    int addCustomer(Customer customer);
    int updateCustomer(Customer customer);
    ArrayList<CustomerCountry> getCountryCount();
    ArrayList<CustomerSpender> getHighestSpenders();
    ArrayList<CustomerGenre> getFavoriteGenre(String id);
}
