package no.noroff.ytunes.repositories;

import no.noroff.ytunes.models.Customer;
import no.noroff.ytunes.models.CustomerCountry;
import no.noroff.ytunes.models.CustomerGenre;
import no.noroff.ytunes.models.CustomerSpender;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private final String URL = ConnectionHelper.CONNECTION_URL;

    /**
     * Gets all the customers in the database.
     * @return customerList a list of all the customers.
     */
    @Override
    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer> customerList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerid, firstname, lastname, " +
                            "country, postalcode, phone, email FROM Customer");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerList.add(new Customer(
                                resultSet.getInt("customerid"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getString("country"),
                                resultSet.getString("postalcode"),
                                resultSet.getString("phone"),
                                resultSet.getString("email")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return customerList;
    }

    /**
     * Gets a specific customer by the id provided.
     * @param id a string with the id to search for.
     * @return customer a customer by the id provided.
     */
    @Override
    public Customer getCustomerById(String id) {
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(URL)){
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstname, lastname, country, postalcode, phone, email " +
                            "FROM Customer WHERE customerId = ?");
            preparedStatement.setString(1,id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customerid"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getString("country"),
                        resultSet.getString("postalcode"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        return customer;
    }

    /**
     * Gets the customers that match the name provided.
     * @param name a string with a name.
     * @return customer a customer object.
     */
    @Override
    public Customer getCustomerByName(String name) {
        Customer customer = null;
        try(Connection conn = DriverManager.getConnection(URL)){
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstname, lastname, country, postalcode, phone, email FROM Customer\n" +
                            "WHERE firstname LIKE ? OR LastName LIKE ?");
            preparedStatement.setString(1,"%" + name + "%");
            preparedStatement.setString(2,"%" + name + "%");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customerid"),
                        resultSet.getString("firstname"),
                        resultSet.getString("lastname"),
                        resultSet.getString("country"),
                        resultSet.getString("postalcode"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        return customer;
    }

    /**
     * Gets a list of customers based on the limit and the offset.
     * @param limit amount of customers shown.
     * @param offset start point for getting the customer amount.
     * @return customerList a list of the customers within the parameters.
     */
    @Override
    public ArrayList<Customer> getCustomerPage(int limit, int offset) {
        ArrayList<Customer> customerList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstname, lastname, country, postalcode, phone, email from Customer\n" +
                            "LIMIT ? OFFSET ?");
            preparedStatement.setInt(1,limit);
            preparedStatement.setInt(2,offset);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customerList.add(new Customer(
                                resultSet.getInt("customerid"),
                                resultSet.getString("firstname"),
                                resultSet.getString("lastname"),
                                resultSet.getString("country"),
                                resultSet.getString("postalcode"),
                                resultSet.getString("phone"),
                                resultSet.getString("email")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return customerList;
    }

    /**
     * Adds a customer to the database.
     * @param customer a customer object.
     * @return result which is either the row count or 0 if it fails.
     */
    @Override
    public int addCustomer(Customer customer) {
        int result = 0;
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer(customerid, firstname, lastname, " +
                            "country, postalcode, phone, email) VALUES(?,?,?,?,?,?,?)");

            preparedStatement.setInt(1,customer.id());
            preparedStatement.setString(2,customer.firstName());
            preparedStatement.setString(3,customer.lastName());
            preparedStatement.setString(4,customer.country());
            preparedStatement.setString(5,customer.postalCode());
            preparedStatement.setString(6,customer.phoneNumber());
            preparedStatement.setString(7,customer.email());

            result = preparedStatement.executeUpdate();
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        return result;
    }

    /**
     * Update for the customer in the database.
     * @param customer a customer object with updated data.
     * @return result either the row count or 0 if it fails.
     */
    @Override
    public int updateCustomer(Customer customer) {
        int result = 0;
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("""
                            UPDATE Customer SET firstname = ?, LastName = ?, country = ?, PostalCode = ?, phone = ?, Email = ? WHERE CustomerID = ?""");

            preparedStatement.setString(1,customer.firstName());
            preparedStatement.setString(2,customer.lastName());
            preparedStatement.setString(3,customer.country());
            preparedStatement.setString(4,customer.postalCode());
            preparedStatement.setString(5,customer.phoneNumber());
            preparedStatement.setString(6,customer.email());
            preparedStatement.setInt(7,customer.id());

            result = preparedStatement.executeUpdate();
        }
        catch (Exception exception){
            System.out.println(exception.toString());
        }
        return result;
    }

    /**
     * Gets the number of customers per country in descending order.
     * @return countryList a list of the countries.
     */
    @Override
    public ArrayList<CustomerCountry> getCountryCount() {
        ArrayList<CustomerCountry> countryList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT country, COUNT(country) as CustomerPerCountry FROM Customer\n" +
                            "GROUP BY country\n" +
                            "ORDER BY CustomerPerCountry DESC");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                countryList.add(new CustomerCountry(
                                resultSet.getString("country"),
                                resultSet.getInt("CustomerPerCountry")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return countryList;
    }

    /**
     * Gets the highest spenders in descending order with the most spending customer at the top.
     * @return spenderList a list of the spenders.
     */
    @Override
    public ArrayList<CustomerSpender> getHighestSpenders() {
        ArrayList<CustomerSpender> spenderList = new ArrayList<>();
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Customer.customerid, (Customer.FirstName || ? || Customer.lastname) AS CustomerName, sum(Invoice.total) AS total FROM Customer\n" +
                            "INNER JOIN Invoice ON Invoice.customerid=Customer.customerid\n" +
                            "GROUP BY Customer.customerid\n" +
                            "ORDER BY total DESC");
            preparedStatement.setString(1, " ");

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                spenderList.add(new CustomerSpender(
                                resultSet.getInt("customerid"),
                                resultSet.getString("CustomerName"),
                                resultSet.getInt("total")
                        )
                );
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return spenderList;
    }

    /**
     * Gets the favorite genre for the customer by the id provided, if more than one returns all matches.
     * @param id the id of the customer.
     * @return genreList a list of the most favorite genre/genres.
     */
    @Override
    public ArrayList<CustomerGenre> getFavoriteGenre(String id) {
        ArrayList<CustomerGenre> genreList = new ArrayList<>();
        int mostTracksByGenre = 0;
        try(Connection conn = DriverManager.getConnection(URL)) {

            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Customer.customerid, (Customer.FirstName || ? || Customer.lastname) as FullName, COUNT(Track.genreid) AS tracksByGenre, Genre.name AS FavoriteGenre\n" +
                            "FROM InvoiceLine\n" +
                            "INNER Join Track ON Track.TrackId=InvoiceLine.trackid\n" +
                            "INNER Join Genre ON Genre.genreid=Track.genreid\n" +
                            "INNER JOIN Invoice ON Invoice.InvoiceId=InvoiceLine.InvoiceId\n" +
                            "INNER JOIN Customer ON Invoice.CustomerId=Customer.customerid\n" +
                            "WHERE Customer.customerid == ?\n" +
                            "GROUP BY Genre.name\n" +
                            "Order BY tracksByGenre DESC");
            preparedStatement.setString(1, " ");
            preparedStatement.setString(2, id);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                if ( resultSet.getInt("tracksByGenre") >= mostTracksByGenre) {
                    genreList.add(new CustomerGenre(
                                    resultSet.getInt("customerid"),
                                    resultSet.getString("FullName"),
                                    resultSet.getInt("tracksByGenre"),
                                    resultSet.getString("FavoriteGenre")
                            )
                    );
                    mostTracksByGenre = resultSet.getInt("tracksByGenre");
                }
            }
        } catch (Exception exception) {
            System.out.println(exception.toString());
        }
        return genreList;
    }

}
