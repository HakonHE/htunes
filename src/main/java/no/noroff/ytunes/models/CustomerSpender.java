package no.noroff.ytunes.models;

public record CustomerSpender(int customerId, String fullName, int totalSpent) {
}
