package no.noroff.ytunes.models;

import java.util.Objects;

public class SongSearch {
    private String keyword;

    public SongSearch() {   }

    public SongSearch(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SongSearch that = (SongSearch) o;
        return Objects.equals(keyword, that.keyword);
    }

    @Override
    public int hashCode() {
        return Objects.hash(keyword);
    }

    @Override
    public String toString() {
        return "SongSearch{" +
                "keyword='" + keyword + '\'' +
                '}';
    }
}