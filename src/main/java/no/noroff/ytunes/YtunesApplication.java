package no.noroff.ytunes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YtunesApplication {

    public static void main(String[] args) {
        SpringApplication.run(YtunesApplication.class, args);
    }

}
