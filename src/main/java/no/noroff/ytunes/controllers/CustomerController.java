package no.noroff.ytunes.controllers;

import no.noroff.ytunes.models.Customer;
import no.noroff.ytunes.models.CustomerCountry;
import no.noroff.ytunes.models.CustomerGenre;
import no.noroff.ytunes.models.CustomerSpender;
import no.noroff.ytunes.repositories.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("api/v1/customers")
public class CustomerController {
    @Autowired
    private CustomerRepository customerRepository;

    @GetMapping
    public ArrayList<Customer> getAllCustomers() {
        return customerRepository.getAllCustomers();
    }

    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable String id){
        return customerRepository.getCustomerById(id);
    }

    @GetMapping("/")
    public Customer getCustomerByName(@RequestParam String name){
        return customerRepository.getCustomerByName(name);
    }

    @GetMapping("/page/")
    public ArrayList<Customer> getCustomerPage(@RequestParam int limit, int offset){
        return customerRepository.getCustomerPage(limit, offset);
    }

    @GetMapping("/countries")
    public ArrayList<CustomerCountry> getCountryCount() {
        return customerRepository.getCountryCount();
    }

    @GetMapping("/spenders")
    public ArrayList<CustomerSpender> getHighestSpenders() {
        return customerRepository.getHighestSpenders();
    }

    @GetMapping("/{id}/favoritegenre")
    public ArrayList<CustomerGenre> getFavoriteGenre(@PathVariable String id) {
        return customerRepository.getFavoriteGenre(id);
    }

    @PostMapping
    public int addCustomer(@RequestBody Customer customer) {
        return customerRepository.addCustomer(customer);
    }

    @PutMapping
    public int updateCustomer(@RequestBody Customer customer) {
        return customerRepository.updateCustomer(customer);
    }
}
